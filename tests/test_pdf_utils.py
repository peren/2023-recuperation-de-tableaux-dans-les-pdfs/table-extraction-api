# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import os.path
from pathlib import Path

import pytest
from fitz import Document
from numpy import ndarray
from table_extraction import pdf_utils

PROJECT_PATH = Path(os.path.dirname(__file__)).parent.resolve()
TEST_PDF_PATH = PROJECT_PATH / "tests/data/table_extraction.pdf"


@pytest.fixture(name="document")
def get_document():
    doc, _ = pdf_utils.load_pdf_and_get_number_of_pages(filepath=TEST_PDF_PATH)
    return doc


def test_load_pdf_and_get_number_of_pages():
    doc, page_count = pdf_utils.load_pdf_and_get_number_of_pages(filepath=TEST_PDF_PATH)
    assert page_count == 7
    assert isinstance(doc, Document)


def test_load_pdf_page_as_png_info(document):
    png_info = pdf_utils.load_pdf_page_as_png_info(document, 1)
    assert png_info["height"] == 792
    assert isinstance(png_info["image"], ndarray)
    assert png_info["image"].shape == (792, 612, 3)
