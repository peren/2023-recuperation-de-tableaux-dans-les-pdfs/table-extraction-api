"""
Test the extraction procedure.
"""
import os.path
from pathlib import Path
from table_extraction.core import get_all_tables, Flavor, CutoffScenario
from table_extraction.predict_locations import get_tables_location_on_pdf


PROJECT_PATH = Path(os.path.dirname(__file__)).parent.resolve()
TEST_PDF_PATH = PROJECT_PATH / "tests/data/table_extraction.pdf"


def test_get_all_tables():
    tables = get_all_tables(
        filepath=TEST_PDF_PATH,
        flavor=Flavor.STREAM,
        cutoff_scenario=CutoffScenario.COMPROMISE,
        take_screenshot=False,
    )
    # Missing one detected table
    assert len(tables) >= 6


def test_table_detection():
    tables = get_tables_location_on_pdf(
        filepath=TEST_PDF_PATH,
    )
    assert len(tables) == 7
    assert len(tables[0]) == 2
    assert len(tables[1]) == 1
    # Problem on page 2
    assert len(tables[3]) == 1
    assert len(tables[4]) == 1
    assert len(tables[5]) == 1
    assert len(tables[6]) == 1
