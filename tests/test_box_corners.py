# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import pytest
from table_extraction.predict_locations import BoxCorners


def test_transform_coordinates():
    with pytest.raises(ValueError):
        BoxCorners(1, 1, 1, 3).export_to_camelot(2)
    assert BoxCorners(1, 1, 9, 9).export_to_camelot(10) == (1, 9, 9, 1)
