#!/bin/bash

# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

set -e

cd ./tests/data

# Certificate issue
# wget https://www.ccomptes.fr/system/files/2023-07/20230718-S2023-0698-Pass-Culture_0.pdf -O pass_culture.pdf
# pdftk pass_culture.pdf cat 7 output one_page_table.pdf

wget https://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Redmon_You_Only_Look_CVPR_2016_paper.pdf -O  yolo.pdf
wget https://s22.q4cdn.com/959853165/files/doc_downloads/2021/03/2020-SASB-Report_FINAL.pdf -O  netflix.pdf

pdftk A=yolo.pdf N=netflix.pdf cat A6-8 N3 N5 N10 N15 output table_extraction.pdf

# rm pass_culture.pdf
rm yolo.pdf netflix.pdf
cd ../../
