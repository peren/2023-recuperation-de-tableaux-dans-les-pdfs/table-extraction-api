#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

from collections import defaultdict

import re


from api_table_extraction.config import Config

base_dict = Config.schema()
for value in base_dict[
    "properties"
].values():  # remove values that do not fit well in a yaml
    value.pop("env_names", None)
    value.pop("items", None)

by_thematic_dict = defaultdict(dict)
for key, value in base_dict["properties"].items():
    if "title" not in value:
        continue
    group = re.match(r"\[(.*)\]", value["title"])[1]
    value["title"] = re.sub(r"\[(.*)\] ", "", value["title"])
    by_thematic_dict[group][key] = value
yamlable_dict = {key: dict(value) for key, value in by_thematic_dict.items()}

file_with_comments = ""
for section, section_dict in yamlable_dict.items():
    section_start = f"""
############################################
## {section.upper()}
############################################
    """
    file_with_comments = file_with_comments + section_start
    for key, value in section_dict.items():
        comments = [
            f"# {sub_key}: " + re.sub(r"\n", "\n#", str(comment))
            for sub_key, comment in value.items()
            if sub_key != "default"
        ]
        comment_string = "\n".join(comments)
        key_string = f"""
{comment_string}
# {key.upper()}={value.get('default', None)}
        """
        file_with_comments = file_with_comments + key_string

with open(".default_config.env", "w") as f:
    f.write(file_with_comments)
