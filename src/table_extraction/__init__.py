# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import os.path
from pathlib import Path

PROJECT_PATH = Path(os.path.dirname(__file__)).parent.parent.resolve()
