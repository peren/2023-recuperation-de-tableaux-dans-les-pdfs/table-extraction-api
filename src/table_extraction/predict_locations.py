# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List, Optional, Tuple, Union

import numpy as np
from torchvision import transforms
import torch
from transformers import AutoModelForObjectDetection
from PIL import Image

from table_extraction.post_processing import rescale_bbox
from table_extraction import PROJECT_PATH
from table_extraction.pdf_utils import (
    load_pdf_and_get_number_of_pages,
    load_pdf_page_as_png_info,
)


MODEL_PATH = str(PROJECT_PATH / "static/model_zoo/table-transformer-detection/")
DEVICE = "cpu"


class MaxResize(object):
    def __init__(self, max_size=800):
        self.max_size = max_size

    def __call__(self, image):
        width, height = image.size
        current_max_size = max(width, height)
        scale = self.max_size / current_max_size
        resized_image = image.resize(
            (int(round(scale * width)), int(round(scale * height)))
        )

        return resized_image


@dataclass
class BoxCorners:
    top_left_horizontal: int
    top_left_vertical: int
    bottom_right_horizontal: int
    bottom_left_vertical: int

    def export_to_camelot(self, height: int) -> Tuple[int, int, int, int]:
        if height < self.top_left_vertical or height < self.bottom_left_vertical:
            raise ValueError("Height must be greater than vertical coordinates")
        return (
            self.top_left_horizontal,
            height - self.top_left_vertical,
            self.bottom_right_horizontal,
            height - self.bottom_left_vertical,
        )

    def export_to_cropping(self) -> Tuple[int, int, int, int]:
        return (
            self.top_left_horizontal,
            self.top_left_vertical,
            self.bottom_right_horizontal,
            self.bottom_left_vertical,
        )


def load_detr() -> AutoModelForObjectDetection:
    model = AutoModelForObjectDetection.from_pretrained(MODEL_PATH, revision="no_timm")
    model.to(DEVICE)
    return model


def model_outputs_to_boxes(
    outputs, img_size: tuple[int], score_threshold: float
) -> list[tuple[BoxCorners, float]]:
    """
    Format Table Transformer model output.
    """
    m = outputs.logits.softmax(-1).max(-1)
    pred_labels = list(m.indices.detach().cpu().numpy())[0]
    pred_scores = list(m.values.detach().cpu().numpy())[0]
    pred_bboxes = outputs["pred_boxes"].detach().cpu()[0]
    pred_bboxes = [elem.tolist() for elem in rescale_bbox(pred_bboxes, img_size)]

    # Table Transformer can predict non-table objects with label `2`,
    # which we exclude here to only keep detected tables
    return [
        (BoxCorners(*[float(elem) for elem in bbox]), float(score))
        for label, score, bbox in zip(pred_labels, pred_scores, pred_bboxes)
        if (int(label) != 2) and (float(score) > score_threshold)
    ]


def tables_locations_on_png(
    model: AutoModelForObjectDetection, image: np.ndarray, score_threshold=0.1
) -> list[tuple[BoxCorners, float]]:
    # Image array to PIL.Image
    image = Image.fromarray(image)
    # Detection transform
    detection_transform = transforms.Compose(
        [
            MaxResize(800),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        ]
    )
    # Prepare image for inference
    pixel_values = detection_transform(image).unsqueeze(0)
    pixel_values = pixel_values.to(DEVICE)
    # Inference
    with torch.no_grad():
        outputs = model(pixel_values)
    return model_outputs_to_boxes(outputs, image.size, score_threshold)


@dataclass
class TablesLocationInformation:
    location: BoxCorners
    height: int
    image: np.ndarray
    score: float


def get_page_output(
    predictor: AutoModelForObjectDetection,
    document,
    page,
    uuid_key: str,
    score_threshold: float,
) -> List[TablesLocationInformation]:
    png_info = load_pdf_page_as_png_info(document, page, uuid_key=uuid_key)
    return [
        TablesLocationInformation(
            location=table_location,
            height=png_info["height"],
            image=png_info["image"],
            score=table_score,
        )
        for table_location, table_score in tables_locations_on_png(
            predictor, png_info["image"], score_threshold=score_threshold
        )
    ]


def get_tables_location_on_pdf(
    filepath: Optional[Union[str, Path]], uuid_key=None, score_threshold=0.1
) -> Dict[int, List[TablesLocationInformation]]:
    document, page_count = load_pdf_and_get_number_of_pages(filepath=filepath)
    model = load_detr()
    tables_locations_by_page = {
        page: get_page_output(
            model,
            document,
            page,
            uuid_key=uuid_key,
            score_threshold=score_threshold,
        )
        for page in range(page_count)
    }
    del model  # we had some memory leak, and it was probably the predictor
    return tables_locations_by_page
