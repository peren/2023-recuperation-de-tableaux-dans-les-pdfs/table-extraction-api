# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import tempfile
from pathlib import Path
from typing import Tuple, Union
from contextlib import contextmanager

import fitz
import numpy as np

from api_table_extraction.storage import register_zip_state


@contextmanager
def get_filepath_from_file(file: bytes) -> Path:
    # as camelot requires a file path, we give it one
    temp_file = tempfile.NamedTemporaryFile(suffix=".pdf")
    temp_file.write(file)
    try:
        yield Path(temp_file.name)
    finally:
        temp_file.close()


def load_pdf_and_get_number_of_pages(
    filepath: Union[str, Path]
) -> Tuple[fitz.Document, int]:
    doc = fitz.open(filename=filepath, stream=None)
    return doc, doc.page_count


def load_pdf_page_as_png_info(
    document: fitz.Document(), page_number: int, uuid_key=None
):
    if uuid_key is not None:
        register_zip_state(uuid_key, f"detecting tables for page {page_number+1}")
    pixmap = document.get_page_pixmap(page_number)
    # https://stackoverflow.com/a/53066662/19458911
    image = np.frombuffer(pixmap.samples, dtype=np.uint8).reshape(
        pixmap.h, pixmap.w, pixmap.n
    )
    return {"image": image, "height": pixmap.height}
