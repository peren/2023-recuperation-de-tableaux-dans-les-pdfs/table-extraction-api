# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT


def merge_lignes(csv_as_list_of_list, cutoff: int):
    output_csv = []
    for row in csv_as_list_of_list:
        if len([value for value in row if value]) > cutoff or not output_csv:
            output_csv += [row]
        else:
            output_csv[-1] = [
                f"{first_line_element} {second_line_element}"
                for first_line_element, second_line_element in zip(output_csv[-1], row)
            ]
    return output_csv


def filter_bogus_tables(merged_lines):
    """
    heuristics to remove obviously bad parsed tables. If the number of lines/columns is too small or some case too big,
    returns None
    :param merged_lines: csv ready to output
    :return: same csv if the table seems correct, None otherwise
    """
    all_lines = [case for column in merged_lines for case in column]
    if len(all_lines) < 3:
        return None
    for case in all_lines:
        if isinstance(case, str):
            if len(case) > 200:
                return None
    return merged_lines
