# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

# pylint: disable=no-member
import logging
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from typing import Dict, List, Optional, Union

from PIL import Image
from PIL.Image import Image as ImageType
import camelot
from camelot.core import TableList

from table_extraction.output_csv_manipulation import filter_bogus_tables, merge_lignes
from table_extraction.predict_locations import (
    BoxCorners,
    TablesLocationInformation,
    get_tables_location_on_pdf,
)

logger = logging.getLogger(__name__)


class Flavor(Enum):
    STREAM = "stream"
    LATTICE = "lattice"


class CutoffScenario(Enum):
    MAXIMIZE = "maximize"
    MINIMIZE = "minimize"
    COMPROMISE = "compromise"


@dataclass
class ExtractedTableInformation:
    score: float
    screenshot: ImageType
    table: Optional[camelot.core.Table]

    def export_to_csv(self, cutoff_scenario: str, take_screenshot: bool):
        if not self.table:
            return None
        csv_as_list_of_list = self.table.data
        n_columns = max(len(row) for row in csv_as_list_of_list)
        cutoff_by_scenario = {
            CutoffScenario.MINIMIZE.value: 1,
            CutoffScenario.MAXIMIZE.value: max(1, n_columns - 1),
            CutoffScenario.COMPROMISE.value: max(1, n_columns // 2),
        }
        merged_lines = merge_lignes(
            csv_as_list_of_list, cutoff=cutoff_by_scenario[cutoff_scenario]
        )
        output_csv = filter_bogus_tables(merged_lines)
        if output_csv is None:
            return None
        if not take_screenshot:
            return output_csv
        return output_csv, self.screenshot


def read_table_at_location(
    filepath: str, page: int, table_location: BoxCorners, table_height: int, flavor: str
) -> Optional[TableList]:
    logger.info(f"Page {page}")
    tables_areas = ",".join(
        [
            str(round(coordinate))
            for coordinate in table_location.export_to_camelot(table_height)
        ]
    )
    try:
        area_table = camelot.read_pdf(
            filepath,
            pages=str(page + 1),
            flavor=flavor,
            table_areas=[tables_areas],
            suppress_stdout=True,
        )
        logger.info(f"\t- Camelot read {len(area_table)} tables (flavor={flavor})")
        return area_table[0]
    except (ValueError, IndexError) as error:
        # if a table is detected on an area with a non-textual element, camelot may raise these errors
        logger.info(
            f"\t- Camelot encountered '{error}' error, maybe a non-textual element ?"
        )
        return None


def read_all_tables_in_pdf(
    tables_locations_by_page: Dict[int, List[TablesLocationInformation]],
    flavor: str,
    filepath: Optional[Union[str, Path]] = None,
) -> Dict[int, List[ExtractedTableInformation]]:
    return {
        page: [
            ExtractedTableInformation(
                table=read_table_at_location(
                    filepath, page, table.location, table.height, flavor=flavor
                ),
                screenshot=Image.fromarray(table.image).crop(
                    table.location.export_to_cropping()
                ),
                score=table.score,
            )
            for table in tables_locations_by_page[page]
        ]
        for page in tables_locations_by_page
        if len(tables_locations_by_page[page]) >= 1
    }


def get_all_tables(
    filepath: Path,
    flavor: Flavor,
    cutoff_scenario: CutoffScenario,
    take_screenshot=True,
    uuid_key=None,
    score_threshold=0.1,
) -> Dict:
    tables_locations_by_page = get_tables_location_on_pdf(
        filepath, uuid_key=uuid_key, score_threshold=score_threshold
    )
    tables = read_all_tables_in_pdf(
        tables_locations_by_page=tables_locations_by_page,
        filepath=str(filepath),
        flavor=flavor.value,
    )
    all_tables = {
        f"page-{page_number + 1}-table-{table_number}_score_{extracted_table.score:.2f}": (
            extracted_table.export_to_csv(cutoff_scenario.value, take_screenshot)
        )
        for page_number, extracted_table_list in tables.items()
        for table_number, extracted_table in enumerate(extracted_table_list)
    }
    return {
        # we want the scores as floats but the "." in the name do not fit well with files
        table_key.replace(".", "_"): table
        for table_key, table in all_tables.items()
        if table is not None
    }
