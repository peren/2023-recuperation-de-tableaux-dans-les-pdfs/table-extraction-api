"""
Post-processing for Table Transformer outputs.
"""
import torch
from torchvision.ops import box_convert


def rescale_bbox(out_bbox: torch.Tensor, size: tuple[int]):
    """
    Rescale bbox coordinates originally between 0 and 1 for the given image size.
    """
    img_w, img_h = size
    b = box_convert(out_bbox, in_fmt="cxcywh", out_fmt="xyxy")
    b = b * torch.tensor([img_w, img_h, img_w, img_h], dtype=torch.float32)
    return b
