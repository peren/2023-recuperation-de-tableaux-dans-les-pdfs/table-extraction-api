# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import markdown2
from api_table_extraction.config import DOCUMENTATION_DIR, README_PATH
from fastapi import APIRouter, responses, Request
from pathlib import Path

router = APIRouter()


def markdown_file_to_response(
    path: Path, html_before_md="", html_after_md=""
) -> responses.HTMLResponse:
    base_html = markdown2.markdown_path(
        path,
        extras=[
            "cuddled-lists",
            "tables",
            "fenced-code-blocks",
            "header-ids",
            "smarty-pants",
        ],
    )
    page_title = "API scraptablo"
    complete_html = f"""
    <!DOCTYPE html>
    <html data-fr-theme="light" lang="fr-FR">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="generator" content="Hugo 0.80.0">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="/static/css/dsfr.css">
        <link rel="stylesheet" href="/static/css/monokai.css">
        <link rel="stylesheet" href="/static/css/style.css">
        <title>{page_title}</title>
        <meta name="description" content=" ">
        <link rel="icon" href="/static/img/marianne.png" type="image/png">
        <link rel="icon" href="/static/img/marianne.svg">
        <link rel="icon" href="/static/img/favicon-32.png" type="image/png" sizes="32x32">
        <link rel="icon" href="/static/img/favicon-64.png" type="image/png" sizes="64x64">
    </head>
    <body>
        <header role="banner" class="fr-header">
        <div class="fr-header__body">
            <div class="fr-container">
                <div class="fr-header__body-row">
                    <div class="fr-header__brand fr-enlarge-link">
                        <div class="fr-header__brand-top">
                            <div class="fr-header__logo">
                                <p class="fr-logo">Gouvernement</p>
                            </div>
                        </div>
                        <div class="fr-header__service">
                                <div class="fr-header__service-title">{page_title}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="fr-container" id="mainContainer" role="main">
        <main id="homepage">
            {html_before_md}
            {base_html}
            {html_after_md}
        </main>
    </div>
    </body>
    </html>
    </html>"""
    return responses.HTMLResponse(complete_html)


def redirect_to_interactive_doc(request: Request):
    """Simple html div to redirect to the interactive documentation of the API"""
    docs_url = request.scope["root_path"] + "docs"
    return f"""
    <div style="width: 100%; text-align: center">
        <a href="{docs_url}">Cliquer ici pour accéder à la documentation interactive</a>
    </div>
    """


@router.get("/docs/{doc_file}", response_class=responses.HTMLResponse)
def get_doc_file(doc_file: str):
    """Retrieve all files in the `/docs directory` and create route"""
    return markdown_file_to_response(DOCUMENTATION_DIR / doc_file)


@router.get("/", response_class=responses.HTMLResponse)
def get_main_readme(request: Request):
    before_md_redirection = redirect_to_interactive_doc(request)
    return markdown_file_to_response(README_PATH, html_before_md=before_md_redirection)


@router.get("/README.md", response_class=responses.RedirectResponse)
def redirect_main_readme(request: Request):
    """Redirect requests from `/README.md` to `/`. Used for internal references in .md files."""
    return responses.RedirectResponse(request.scope["root_path"])
