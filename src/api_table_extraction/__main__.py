# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import argparse
from typing import Dict, Optional
import logging

import uvicorn

from .routes import APP


class UnknownCommandError(Exception):
    pass


def make_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        prog="{{ api_table_extraction }}", description="{{ project_description }}"
    )

    subparsers = parser.add_subparsers(dest="cmd")

    serve_parser = subparsers.add_parser(
        "serve", help="Serve the API in development mode."
    )
    serve_parser.add_argument("--host", default="127.0.0.1")
    serve_parser.add_argument("--port", type=int, default=3000)
    return parser


def run_with_args(args: argparse.Namespace) -> Optional[Dict]:
    if args.cmd == "serve":
        config = uvicorn.Config(APP, host=args.host, port=args.port, log_level="info")
        server = uvicorn.Server(config)
        logging.getLogger("uvicorn.info")
        server.run()
        return
    else:
        raise UnknownCommandError(f"Unknown command: {args.cmd}")


if __name__ == "__main__":
    parser = make_parser()
    args = parser.parse_args()
    try:
        run_with_args(args)
    except UnknownCommandError:
        parser.print_help()
