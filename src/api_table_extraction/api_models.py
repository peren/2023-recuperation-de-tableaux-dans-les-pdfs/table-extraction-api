# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

from pydantic import BaseModel  # pylint: disable=E0611


class APIOutput(BaseModel):  # pylint: disable=E1139
    pass


class PDFInput(BaseModel):  # pylint: disable=E1139
    pass
