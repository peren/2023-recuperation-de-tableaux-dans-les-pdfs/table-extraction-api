# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import re
from typing import Annotated, Dict, List

from fastapi import APIRouter, File, BackgroundTasks, Query, HTTPException
from fastapi.responses import FileResponse

from table_extraction.pdf_utils import get_filepath_from_file
from .config import env_config
from .extract import extract_and_save_tables
from .storage import (
    ZipNotFoundError,
    generate_storage_path,
    generate_user_task_uuid_dict,
    get_model_info,
    get_model_state,
    merge_user_task_uuid,
    Header,
)
from table_extraction.core import (
    CutoffScenario,
    Flavor,
    get_all_tables,
)

scraping_router = APIRouter()


@scraping_router.post(
    "/extract_table",
    response_model=Dict[str, List[List[str]]],
    summary="pdf vers json",
    description="extrait les tables du pdf en entrée et donne un json en sortie",
)
def extract_table(
    file: Annotated[bytes, File()],
    flavor: Flavor = Flavor.STREAM,
    cutoff_scenario: CutoffScenario = CutoffScenario.COMPROMISE,
):
    with get_filepath_from_file(file) as pathfile:
        return get_all_tables(
            pathfile,
            flavor=flavor,
            cutoff_scenario=cutoff_scenario,
            take_screenshot=False,
        )


@scraping_router.post(
    "/async_extract_table_to_files",
    response_model=str,
    summary="extrait les tables du pdf en entrée",
)
async def async_extract_table_to_files(
    file: Annotated[bytes, File()],
    background_tasks: BackgroundTasks,
    x_user: str = Header("Unknown"),
    table_detection_score_threshold: float = 0.1,
    flavor: Flavor = Flavor.STREAM,
    cutoff_scenario: CutoffScenario = CutoffScenario.COMPROMISE,
):
    """
    Lance une extraction de tables sur le ficher fourni.
    On peut augmenter le table_detection_score_threshold pour réduire le nombre de tables détectés.
    Cela retourne en sortie un uuid qui peut être utilisé pour suivre l'avancement de l'extraction et pour récupérer
    l'archive une fois que c'est fini
    """
    uuid_dict = generate_user_task_uuid_dict(x_user)
    output_dir = generate_storage_path(x_user)
    background_tasks.add_task(
        extract_and_save_tables,
        file,
        output_dir,
        uuid_dict["merged"],
        table_detection_score_threshold,
        flavor,
        cutoff_scenario,
    )
    return uuid_dict["task_uuid"]


UUID_RE = re.compile(
    "^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}$"
)


@scraping_router.post(
    "/get_extraction_status",
    summary="donne l'état d'extraction",
)
def get_extraction_status(
    task_id: str = Query(..., regex=UUID_RE.pattern), x_user: str = Header("Unknown")
):
    task = merge_user_task_uuid(x_user, task_id)
    try:
        message = get_model_state(task)
    except ZipNotFoundError:
        raise HTTPException(404)

    return message


@scraping_router.post(
    "/get_files",
    response_class=FileResponse,
    summary="donne l'archive correspondante",
)
def get_files(
    task_id: str = Query(..., regex=UUID_RE.pattern), x_user: str = Header("Unknown")
):
    task = merge_user_task_uuid(x_user, task_id)
    try:
        info_dict = get_model_info(task)
    except ZipNotFoundError:
        raise HTTPException(404, detail="zip file not found, wait a bit more")
    output_location = env_config.USER_ZIP_DIR / info_dict["path"] / "table.zip"
    return FileResponse(
        output_location,
        media_type="application/x-zip-compressed",
        headers={"Content-Disposition": "attachment;filename=summary.zip"},
    )
