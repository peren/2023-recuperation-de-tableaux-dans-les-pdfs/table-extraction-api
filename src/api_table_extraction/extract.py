# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import csv
import tempfile
from pathlib import Path

from zipfile import ZIP_DEFLATED, ZipFile

from api_table_extraction.storage import (
    register_zip,
    register_zip_state,
)
from table_extraction.core import (
    CutoffScenario,
    Flavor,
    get_all_tables,
)
from table_extraction.pdf_utils import get_filepath_from_file


def zip_files(filenames, output_location="test.zip"):
    zipper = ZipFile(output_location, mode="w", compression=ZIP_DEFLATED)
    for filename in filenames:
        zipper.write(filename, arcname=filename.name)
    zipper.close()


def write_zip_from_csv_list(dict_list_csv_as_list, output_dir, uuid_key):
    output_location = output_dir / "table.zip"
    with tempfile.TemporaryDirectory() as temp_dir:  # temporary dir to group files to compress
        dir_path = Path(temp_dir).resolve()
        file_list = []
        for csv_name, (reversed_csv, screenshot) in dict_list_csv_as_list.items():
            base_name = Path(f"{dir_path}/{csv_name}")
            table = list(zip(*reversed_csv))
            file_name = base_name.with_suffix(".csv")
            file_list.append(file_name)
            with open(file_name, "w") as csv_to_write:
                csv_writer = csv.writer(csv_to_write, delimiter=";")
                csv_writer.writerows(table)
            image_name = base_name.with_suffix(".png")
            file_list.append(image_name)
            screenshot.save(image_name)
        zip_files(file_list, output_location=output_location)
    register_zip(uuid_key, output_dir)
    register_zip_state(uuid_key, "files written")


def extract_and_save_tables(
    file: bytes,
    output_dir,
    uuid_key: str,
    score_threshold: float,
    flavor: Flavor,
    cutoff_scenario: CutoffScenario,
):
    register_zip_state(uuid_key, "extracting")
    with get_filepath_from_file(file) as pathfile:
        dict_list_csv_as_list = get_all_tables(
            pathfile,
            flavor=flavor,
            cutoff_scenario=cutoff_scenario,
            uuid_key=uuid_key,
            score_threshold=score_threshold,
        )
    register_zip_state(uuid_key, "extraction finished")
    write_zip_from_csv_list(dict_list_csv_as_list, output_dir, uuid_key)
