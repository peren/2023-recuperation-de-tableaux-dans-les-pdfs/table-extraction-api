# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

import time
import uuid
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp
from typing import NewType, TypedDict

import redis

from api_table_extraction.config import env_config

from fastapi import Header


def HiddenHeader(*args, **kwargs):
    return Header(*args, include_in_schema=False, **kwargs)


def UserHeader(*args, **kwargs):
    return HiddenHeader(..., *args, alias=env_config.PROXY_USER_HEADER_NAME, **kwargs)


class ZipNotFoundError(Exception):
    pass


def get_redis_user_storage():
    return redis.Redis.from_url(env_config.REDIS_HOSTNAME)


REDIS_STORAGE = get_redis_user_storage()
StoragePath = NewType("StoragePath", Path)


def generate_storage_path(username: str) -> StoragePath:
    user_directory = env_config.USER_ZIP_DIR / username
    user_directory.mkdir(exist_ok=True)
    tmp_dir = mkdtemp(dir=user_directory)
    return StoragePath(Path(tmp_dir))


def register_zip(zip_id, storage_path: StoragePath):
    storage_dict = {
        "path": str(storage_path.relative_to(env_config.USER_ZIP_DIR)),
    }
    REDIS_STORAGE.hset(zip_id, mapping=storage_dict)
    REDIS_STORAGE.expire(zip_id, env_config.USER_ZIP_TTL)


def register_zip_state(zip_id, message):
    key = f"{zip_id}_state"
    REDIS_STORAGE.set(key, value=message)
    REDIS_STORAGE.expire(key, env_config.USER_ZIP_TTL)


def get_model_state(zip_id):
    key = f"{zip_id}_state"
    query_result = REDIS_STORAGE.get(key)
    if not query_result:
        raise ZipNotFoundError(key)

    return query_result


def get_model_info(zip_id):
    query_result = REDIS_STORAGE.hgetall(zip_id)
    if not query_result:
        raise ZipNotFoundError(zip_id)

    def decode(element):
        return element.decode("utf-8") if isinstance(element, bytes) else element

    info_dict = {decode(k): decode(v) for k, v in query_result.items()}
    return info_dict


def get_zip_path(model_id) -> StoragePath:
    model_info = get_model_info(model_id)
    return StoragePath(env_config.USER_ZIP_DIR / model_info["path"])


def get_all_zip_paths() -> list[StoragePath]:
    return [get_zip_path(model_id) for model_id in REDIS_STORAGE.keys()]


def clean_old_zip() -> int:
    limit_time = time.time() - env_config.USER_ZIP_TTL
    nb_clean_models = 0
    files_to_keep = get_all_zip_paths()

    for user_dir in env_config.USER_ZIP_DIR.iterdir():
        for path in user_dir.iterdir():
            if path.stat().st_mtime < limit_time and path not in files_to_keep:
                if path.is_dir():
                    rmtree(path)
                else:
                    path.unlink()
                nb_clean_models += 1

    return nb_clean_models


class UUIDDict(TypedDict):
    user: str
    task_uuid: str
    merged: str


def merge_user_task_uuid(username: str, task_uuid: str) -> str:
    return f"{username}{env_config.USERNAME_SEPARATOR}{task_uuid}"


def generate_user_task_uuid(username: str) -> str:
    task_uuid = str(uuid.uuid4())
    return merge_user_task_uuid(username, task_uuid)


def generate_user_task_uuid_dict(username: str) -> UUIDDict:
    task_uuid = str(uuid.uuid4())
    return {
        "user": username,
        "task_uuid": task_uuid,
        "merged": merge_user_task_uuid(username, task_uuid),
    }


def split_user_task_uuid(user_task_uuid: str) -> tuple[str, str]:
    user, task_uuid = user_task_uuid.split(env_config.USERNAME_SEPARATOR)
    return user, task_uuid
