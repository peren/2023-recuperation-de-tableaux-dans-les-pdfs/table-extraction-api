# SPDX-FileCopyrightText: 2023 Pôle d'Expertise de la Régulation Numérique <peren.contact@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

from pathlib import Path
from typing import Optional

from pydantic import Field
from pydantic_settings import BaseSettings


PACKAGE_PATH = Path(__file__).resolve().parent
SRC_PATH = PACKAGE_PATH.parent
_ROOT_PATH = (
    PACKAGE_PATH.parent.parent
)  # Accessible from clone of the project, not from package
DOCUMENTATION_DIR = _ROOT_PATH / "docs"
README_PATH = _ROOT_PATH / "README.md"


class Config(BaseSettings):
    verbose: bool = Field(
        False,
        title="[general] Verbose logging enabling",
        description="Enable/disable verbose logging. "
        "You might want to set it to true if you want more information on errors",
    )
    REDIS_HOSTNAME: str = Field(
        "redis://localhost",
        title="[redis] Redis URI",
        description="Redis uri. " "Allow you to be asynchronous while handling parsing",
    )
    USER_ZIP_DIR: Optional[Path] = Field(
        None,
        title="[result_recovery] zipped files directory",
        description="Path to the generic folder that will contain the zipped files",
    )
    USER_ZIP_TTL: int = Field(
        7 * 24 * 3600,
        title="[result_recovery] zip files TTL",
        description="Time to live for the files stored on the zip file directory",
    )
    PROXY_USER_HEADER_NAME: str = Field(
        "X-User",
        title="[general] header name for the alias proxy",
        description="header name for the alias proxy",
    )
    USERNAME_SEPARATOR: str = Field(
        "::",
        title="[result_recovery] username separator",
        description="username separator used for the ids in the stored database",
    )

    class Config:
        env_file = ".env"
        schema_extra = {
            "description": "Handle the environment variable for the app configuration"
        }


env_config = Config()
if env_config.USER_ZIP_DIR is None:
    tmp = Path("/tmp")
    program_dir = tmp / "scraptablo_data"
    program_dir.mkdir(exist_ok=True)
    env_config.USER_ZIP_DIR = program_dir
