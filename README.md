# Scraptablo API V 1.2

Ce dépôt comprend un outil d'extraction de tableaux pour fichier `.pdf`, proposé sous forme d'API.

## Utilisation

### Configuration

Pour télécharger le modèle de détection de tableau lors du `git clone` il faut avoir installé `git-lfs`.

Pour installer les dépendances système nécessaires pour faire tourner les modèles :

```bash
$ apt-get update && apt-get install -y ffmpeg libsm6 libxext6 pdftk
```

Puis pour installer les dépendances python (`poetry` doit être installé ou à défaut `pipx`):

```bash
# Si besoin d'installer `poetry` :
# $ pipx install poetry==1.3.2
$ poetry install
```

### Lancement de l'API

Utilisation en ligne de commande :

```bash
$ .venv/bin/python -m api_table_extraction serve
```

## Développement

### Collaboration

Installer les dépendances de développement et installer les *hooks* de `pre-commit`.

```bash
$ poetry run pre-commit install
```

### Données

Télécharger les données de tests dans `tests/data/` avec le script suivant `$ bash scripts/download_pdf_assets.sh`.

## `table_extraction`, extraction de table

Le parsing se fait en plusieurs étapes :
- récupération des métadonnées sur le pdf avec `fitz` ;
- détection des bordures des tables dans les documents avec un modèle de machine learning ;
- récupération des tables.

### Détection des bordures

La détection des bordures des tables est faite à partir d'un modèle [DETR](https://huggingface.co/microsoft/table-transformer-detection) entraîné sur le dataset [PubTables-1M](https://arxiv.org/abs/2110.00061).
Le modèle est stocké avec git-lfs.

### Récupération des tables

À partir de ces positions, le pdf est parsé avec l'outil de détection de tables sur pdf `camelot` et les tables sont exportées au format csv (un `.csv` par table détectée).

Le mode _stream_ de Camelot est beaucoup plus efficace pour notre outil, il faut utiliser cette valeur de `flavor` de préférence au sein de l'outil.


### Poursuites envisageables

L'outil rencontre peu de faux positifs et de faux négatifs avec le mode `stream` de camelot. Si une "case" s'étend sur plusieurs lignes, on aura cependant également ces sauts de lignes dans le `.csv` final. Il est possible d'affiner les heuristiques existantes pour réduire les faux positifs.

Pour extraire des tableaux à partir de fichiers en format image, il faut compléter le modèle de détection de tables, par un modèle de reconnaissance de structure et le coupler en sortie avec un OCR.

## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte de la licence se trouve dans le fichier [`LICENSE.md`](MIT.txt).

- les fichiers des modèles sont sous licence MIT ;
- quelques lignes de codes sont empruntées et citées de StackOverFLow sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) ;
- les émojis pour l'avatar du projet et du groupe GitLab sont empruntés d'[OpenMoji](https://openmoji.org/) (_the open-source emoji and icon project_) sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/#).